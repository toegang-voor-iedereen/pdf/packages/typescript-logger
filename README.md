# Logger for Typescript

This repository contains a base logger that logs in ECS format. This logger is based on Pino.

Documentation for Pino can be found [here](https://github.com/pinojs/pino/blob/master/docs/api.md)

## How to use

Add an .npmrc file to your project containing:

```
@toegang-voor-iedereen:registry=https://gitlab.com/api/v4/groups/60879101/-/packages/npm
```

Then install this package by running: `npm install @toegang-voor-iedereen/typescript-logger`

You can now import logger in your project!

### Example

```
import { logger } from '@toegang-voor-iedereen/typescript-logger'

logger.child({foo: 'bar'}).info('baz')
```