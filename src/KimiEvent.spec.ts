import { ZodError } from 'zod'
import { KimiEvent, kimiEvent } from './KimiEvent.js'
import { Logger } from './Logger.js'

const fakeLogger = {
    child: vi.fn(() => {
        return fakeLogger
    }),
    info: vi.fn(),
    warn: vi.fn(),
} as unknown as Logger

describe('Event', () => {
    const testDate = new Date('2023-01-01')
    const testDateMinus10Seconds = new Date(testDate.getTime() - 10000)

    it('Should parse a KimiEvent with a string as date', () => {
        const parsedEvent = KimiEvent.safeParse({
            name: 'SomeKimiEvent',
            eventTime: testDate,
            eventStartTime: testDateMinus10Seconds,
        })
        expect(parsedEvent.success).toBe(true)

        if (parsedEvent.success) {
            expect(parsedEvent.data).toMatchInlineSnapshot(`
              {
                "eventStartTime": 2022-12-31T23:59:50.000Z,
                "eventTime": 2023-01-01T00:00:00.000Z,
                "name": "SomeKimiEvent",
              }
            `)
            expect(parsedEvent.data.eventTime).toBeInstanceOf(Date)
        }
    })

    it('Should parse a KimiEvent with a Date instance as date', () => {
        const parsedEvent = KimiEvent.safeParse({
            name: 'SomeKimiEvent',
            eventTime: testDate,
            eventStartTime: testDateMinus10Seconds,
        })
        expect(parsedEvent.success).toBe(true)

        if (parsedEvent.success) {
            expect(parsedEvent.data).toMatchInlineSnapshot(`
              {
                "eventStartTime": 2022-12-31T23:59:50.000Z,
                "eventTime": 2023-01-01T00:00:00.000Z,
                "name": "SomeKimiEvent",
              }
            `)
            expect(parsedEvent.data.eventTime).toBeInstanceOf(Date)
        }
    })

    it('Accepts additional unspecified fields in the event', () => {
        const parsedEvent = KimiEvent.passthrough().safeParse({
            name: 'SomeKimiEvent',
            eventTime: testDate,
            eventStartTime: testDateMinus10Seconds,
            someCoolField: 'foo',
        })
        expect(parsedEvent.success).toBe(true)

        if (parsedEvent.success) {
            expect(parsedEvent.data).toMatchInlineSnapshot(`
              {
                "eventStartTime": 2022-12-31T23:59:50.000Z,
                "eventTime": 2023-01-01T00:00:00.000Z,
                "name": "SomeKimiEvent",
                "someCoolField": "foo",
              }
            `)
        }
    })

    it('Logs an event', () => {
        const parsedEvent = KimiEvent.passthrough().safeParse({
            name: 'SomeKimiEvent',
            eventTime: testDate,
            eventStartTime: testDateMinus10Seconds,
            someCoolField: 'foo',
        })
        expect(parsedEvent.success).toBe(true)

        if (parsedEvent.success) {
            kimiEvent(fakeLogger, parsedEvent.data)

            expect(fakeLogger.child).toHaveBeenLastCalledWith({
                kimiEvent: {
                    name: 'SomeKimiEvent',
                    durationSeconds: 10,
                    eventTime: '2023-01-01T00:00:00.000Z',
                    someCoolField: 'foo',
                },
            })
            expect(fakeLogger.info).toHaveBeenLastCalledWith('kimiEvent.SomeKimiEvent')
        }
    })

    it('Logs a warning if an event cannot be parsed', () => {
        kimiEvent(fakeLogger, {
            name: 5, // this is wrong, so this should make things fail
            eventTime: testDate,
            eventStartTime: testDateMinus10Seconds,
            someCoolField: 'foo',
        } as unknown as KimiEvent)
        expect(fakeLogger.warn).toHaveBeenLastCalledWith(
            expect.any(ZodError),
            'Could not parse event'
        )
    })
})
