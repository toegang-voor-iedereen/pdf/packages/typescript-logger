import { z } from 'zod'
import { Logger } from './Logger.js'

export const KimiEvent = z.object({
    name: z.string(),
    eventTime: z.date().or(z.string().datetime({ offset: true }).pipe(z.coerce.date())),
    eventStartTime: z
        .date()
        .or(z.string().datetime({ offset: true }).pipe(z.coerce.date()))
        .optional(),
    jobId: z.string().optional(),
})

export type KimiEvent = z.infer<typeof KimiEvent>

export const kimiEvent = (logger: Logger, event: KimiEvent) => {
    const parsedEvent = KimiEvent.passthrough().safeParse(event)
    if (parsedEvent.success) {
        const { name, eventStartTime, eventTime, ...rest } = parsedEvent.data
        const durationSeconds = eventStartTime
            ? (eventTime.getTime() - eventStartTime.getTime()) / 1000
            : undefined
        logger
            .child({
                kimiEvent: {
                    name,
                    durationSeconds,
                    eventTime: eventTime.toISOString(),
                    ...rest,
                },
            })
            .info(`kimiEvent.${name}`)
    } else {
        logger.warn(parsedEvent.error, 'Could not parse event')
    }
}
