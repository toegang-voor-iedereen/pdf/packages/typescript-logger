import { logger } from './Logger.js'

describe('Logger', () => {
    it.each(['info', 'trace', 'error', 'warn', 'debug', 'fatal', 'child'])(
        'should have log method: %s',
        async (method) => {
            expect(logger).toHaveProperty(method)
            const childLogger = logger.child({ foo: 'bar' })
            expect(childLogger).toHaveProperty(method)
        }
    )
})
