import * as packageJson from '#package.json' with { type: 'json' }
import { ecsFormat } from '@elastic/ecs-pino-format'
import { Logger as PinoLogger, pino } from 'pino'

const LOGGING_ENABLED: boolean = JSON.parse(process.env.LOGGING_ENABLED ?? 'true')
const TRACE_ENABLED: boolean = JSON.parse(process.env.TRACE_ENABLED ?? 'false')

export type Logger = PinoLogger
export const logger: PinoLogger = pino({
    ...ecsFormat(),
    enabled: LOGGING_ENABLED,
    level: TRACE_ENABLED ? 'trace' : 'debug',
}).child({
    loggerVersion: packageJson.default.version ?? '0.0',
    environment: process.env.ENVIRONMENT ?? 'development',
    nodeVersion: process.env.NODE_VERSION,
})

export type TraceArgs = {
    traceId: string
    logger: Logger
}
