# Changelog

## [1.0.9](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/1.0.8...1.0.9) (2025-02-21)


### Bug Fixes

* **deps:** update dependency eslint-import-resolver-typescript to v3.8.3 ([7881c08](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/7881c08d6f99c16f383dce69f6c8db2f5d15eb17))

## [1.0.8](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/1.0.7...1.0.8) (2025-02-19)


### Bug Fixes

* **deps:** update dependency eslint-import-resolver-typescript to v3.8.2 ([d227e3b](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/d227e3b62094983ca8af6b1578cf860fbf2dd3e3))
* **deps:** update vitest monorepo to v3.0.6 ([13f909c](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/13f909c79a978f990e3e022ee3f907c0af4b2f30))

## [1.0.7](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/1.0.6...1.0.7) (2025-02-18)


### Bug Fixes

* **deps:** update dependency @typescript-eslint/eslint-plugin to v8.24.1 ([11b0cdf](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/11b0cdfb46d388034c5d51da389c5ef54ae84419))
* **deps:** update dependency eslint-import-resolver-typescript to v3.8.1 ([ae66975](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/ae6697599d536d0f4b8a28819590eced5d1d4b2f))

## [1.0.6](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/1.0.5...1.0.6) (2025-02-14)


### Bug Fixes

* **deps:** update dependency @types/node to v22.13.4 ([b84fafd](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/b84fafd9d853d2b6dd12e172d2219b158c940159))
* **deps:** update dependency prettier to v3.5.1 ([d2cb61d](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/d2cb61da12c8e7ff264c8a207887805d03f400e9))

## [1.0.5](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/1.0.4...1.0.5) (2025-02-13)


### Bug Fixes

* **deps:** update dependency @types/node to v22.13.2 ([1427ff4](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/1427ff4f7692568f449059b0c598d658113d3857))

## [1.0.4](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/1.0.3...1.0.4) (2025-02-12)


### Bug Fixes

* **deps:** update dependency zod to v3.24.2 ([24f8ec8](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/24f8ec881617b23dc0ab7ec1a1da1fbd7244e588))

## [1.0.3](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/1.0.2...1.0.3) (2025-02-04)


### Bug Fixes

* **deps:** update dependency @types/node to v22.13.1 ([10bca53](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/10bca539d2373e7fc33f3c635f52aa0ac0752701))
* **deps:** update vitest monorepo to v3.0.5 ([c037d56](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/c037d56ac49bff0a23c54b687e78211e2283f87a))

## [1.0.2](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/1.0.1...1.0.2) (2025-01-24)


### Bug Fixes

* **deps:** update dependency @types/node to v22.10.10 ([09b3965](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/09b3965fcda2cfa4326e9109afd5c276b414b36a))

## [1.0.1](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/1.0.0...1.0.1) (2025-01-23)


### Bug Fixes

* **deps:** update dependency @types/node to v22.10.9 ([09af200](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/09af2002bb0c1a7dfacfd2af2b9eb907efc8cd6f))
* **deps:** update vitest monorepo to v3.0.4 ([0f27a25](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/0f27a255709903b9bfa8f0fa771a21323b5194c3))

# [1.0.0](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/0.1.11...1.0.0) (2025-01-06)


### Features

* node 22 ([7ee4e4f](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/7ee4e4f12e933afaff4132a57a0a6ff83afb153d))


### BREAKING CHANGES

* update to node 22, which makes this package incompatible with earlier node versions.

## [0.1.11](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/0.1.10...0.1.11) (2024-12-30)


### Bug Fixes

* **deps:** update dependency pino to v9.6.0 ([63df9ee](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/63df9eebab0cc58e1f440067c2e04d28aa26141f))
* **deps:** update dependency zod to v3.24.1 ([87599a4](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/87599a444b924156a71b9c78172cf5658b308892))

## [0.1.10](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/0.1.9...0.1.10) (2024-07-26)


### Bug Fixes

* **deps:** update dependency pino to v9.3.2 ([4bde9d0](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/4bde9d0b01fc3da727dca38044490d5b120eedbe))

## [0.1.9](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/0.1.8...0.1.9) (2024-07-17)


### Bug Fixes

* **deps:** update dependency pino to v9.3.1 ([13a7ec1](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/13a7ec147f9a3e273fd0f8a86511395735f7a5f3))

## [0.1.8](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/0.1.7...0.1.8) (2024-06-19)


### Bug Fixes

* **deps:** update dependency pino to v9.2.0 ([13e98ee](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/13e98ee330104213eda31b672dcbd09084b4e820))

## [0.1.7](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/0.1.6...0.1.7) (2024-05-13)


### Bug Fixes

* **deps:** update dependency pino to v9.1.0 ([c0b8c9b](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/c0b8c9bf875205a0381e214f0c2ed15e381ab27f))

## [0.1.6](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/0.1.5...0.1.6) (2024-05-13)


### Bug Fixes

* **deps:** update dependency zod to v3.23.8 ([a77d383](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/a77d383a1909872be9455537aba57cc6a3e11bd8))

## [0.1.5](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/0.1.4...0.1.5) (2024-05-08)


### Bug Fixes

* **deps:** update dependency zod to v3.23.7 ([c94d970](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/c94d9705d25b57615a8d632ca13150957e80e3bf))

## [0.1.4](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/0.1.3...0.1.4) (2024-05-03)


### Bug Fixes

* **deps:** update dependency zod to v3.23.6 ([c7b8270](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/c7b827005e9524e0dde9eaf29e9c32001b995cdc))

## [0.1.3](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/0.1.2...0.1.3) (2024-04-30)


### Bug Fixes

* **deps:** update dependency zod to v3.23.5 ([beb6a35](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/beb6a353c1ffa9f59c276e9faff4a2c4c46a74a8))

## [0.1.2](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/0.1.1...0.1.2) (2024-04-25)


### Bug Fixes

* changed wording ([f6e7896](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/f6e7896720c827dd62ffe9ee351dfeeb13472e1b))

## [0.1.1](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/0.1.0...0.1.1) (2024-04-25)


### Bug Fixes

* added some documentation ([245f7ab](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/245f7ab25ee99b3726e0e14a66a9a852c846b468))

# [0.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/v0.0.12...0.1.0) (2024-04-25)


### Bug Fixes

* **deps:** update dependency pino to v8.20.0 ([06b7668](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/06b76683757c6303a0c88958a3444d2c823f664f))
* **deps:** update dependency pino to v9 ([5c2bdc5](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/5c2bdc546741a08d406187a0589e921f1e5e9b7e))
* **deps:** update dependency zod to v3.22.5 ([0f81325](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/0f81325acce0ae845470bdddd480d6133a6e629e))
* **deps:** update dependency zod to v3.23.0 ([1c2334d](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/1c2334d80f29de3c4af7213928b0d400e5b51000))
* **deps:** update dependency zod to v3.23.3 ([d9e6c62](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/d9e6c621fd3b05683c64cd71fe837bddd2ffdd21))
* **deps:** update dependency zod to v3.23.4 ([0856510](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/08565107dd78f068f04dfd7e1393f7738328e0e7))


### Features

* use shared CI pipeline ([017fda5](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/017fda50fcd3e75d9c919f383c990e5b0a01f2fc))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.12](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/v0.0.11...v0.0.12) (2024-03-28)


### Features

* upgrade logger package ([c817834](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/c8178344a650f2701529d74458e7746aa18f5fbc))

### [0.0.11](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/v0.0.10...v0.0.11) (2023-09-25)


### Features

* introduce eventStartTime field ([221c51f](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/221c51fd68feabe7bc8080495f119521c4bded34))

### [0.0.10](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/v0.0.9...v0.0.10) (2023-09-25)


### Features

* add kimiEvent method ([7696044](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/76960443fb80ea5761864445140f0d050c7f9b25))

### [0.0.9](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/v0.0.8...v0.0.9) (2023-09-20)


### Features

* add env and node version ([c555848](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/c5558489563ac8cbcbd928a417265d5212ccaee6))

### [0.0.8](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/v0.0.7...v0.0.8) (2023-09-20)


### Bug Fixes

* use regular level names ([ac28f10](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/ac28f1044631128e8423bee28bee4f89f6bb9413))

### [0.0.7](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/v0.0.6...v0.0.7) (2023-09-20)


### Features

* expose Logger type instead of KimiLogger ([36869cc](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/36869cc1b7be84b2d65c0756e3a091c1b88f9ed3))

### [0.0.6](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/v0.0.5...v0.0.6) (2023-09-20)


### Features

* default disable trace logging ([4aec297](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/4aec2975c5229bbc5cf43eb8b76b67ce03fa1c04))

### [0.0.5](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/v0.0.4...v0.0.5) (2023-09-19)


### Features

* add logging enabled env var ([4ce8fb3](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/4ce8fb31f9eafb1e0815a08bab3a2aa09170cdb6))

### [0.0.4](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/v0.0.3...v0.0.4) (2023-09-19)

### [0.0.3](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/compare/v0.0.2...v0.0.3) (2023-09-19)

### 0.0.2 (2023-09-19)


### Features

* initial logger package ([a0f0dea](https://gitlab.com/toegang-voor-iedereen/pdf/packages/typescript-logger/commit/a0f0deaa252b2a1a13486d065207e98de87897f9))
